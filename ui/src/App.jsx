import { Button, Container } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import "./App.css";
import Table from "./components/table.jsx";
import { createPayment } from "./services/payments.js";
import { getSynthesis } from "./services/synthesis.js";

const App = () => {
  /**
   * Hydrate data for the table and set state
   */
  const [data, setData] = useState([]);
  const [paging, setPaging] = useState({});
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    async function fetchData(qs) {
      setDataLoaded(false);
      const synthesizedData = await getSynthesis(qs);
      setData(synthesizedData.body);
      setPaging(synthesizedData.page);
      setDataLoaded(true);
    }

    fetchData("?offset=0&count=10");

    window.addEventListener("newQuery", (e) => {
      fetchData(
        `?offset=${e.detail.page * e.detail.count}&count=${e.detail.count}`
      );
    });
  }, []);

  const initiatePayment = async ({ applicationUuid, requestedAmount }) => {
    if (applicationUuid || requestedAmount) {
      await createPayment({
        applicationUuid,
        requestedAmount
      });

      window.dispatchEvent(new CustomEvent("newQuery", {}));
    }
  };

  let tableData = [];
  if (dataLoaded) {
    tableData = data.map((row) => {
      row.initiatePayment = row.eligible ? (
        <Button
          onClick={() =>
            initiatePayment({
              applicationUuid: row.applicationUuid,
              requestedAmount: row.requestedAmount
            })
          }
          variant="contained">
          Pay
        </Button>
      ) : null;
      delete row.eligible;
      delete row.applicationUuid;
      return row;
    });
  }

  return (
    <div className="App">
      <Container>
        {dataLoaded && <Table data={tableData} paging={paging} />}
      </Container>
    </div>
  );
};

export default App;
