import { render, screen, waitFor } from "@testing-library/react";
import App from "./App";

test("table renders with headers", async () => {
  render(<App />);
  await waitFor(() => screen.getByRole("table"));
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

test("payment button only appears when valid application info", async () => {
  render(<App />);

  await waitFor(() => screen.getByRole("table"));

  const tableBody = document.querySelector(".MuiTableBody-root");

  const getFiberNode = (el) => {
    for (const key in el) {
      if (key.startsWith("__reactFiber$")) {
        return el[key].return;
      }
    }
  };

  const getCellVal = (row, id) => {
    return getFiberNode(row.children[id]).child.pendingProps.children;
  };

  const allowPayment = Array.from(tableBody.children).filter((child) => {
    return (
      getCellVal(child, 3) != null &&
      getCellVal(child, 3).length > 0 &&
      getCellVal(child, 5) == null
    );
  });

  const denyPayment = Array.from(tableBody.children).filter((child) => {
    return (
      getCellVal(child, 3) == null ||
      (getCellVal(child, 5) != null && getCellVal(child, 5).length > 0)
    );
  });

  for (let row of allowPayment) {
    expect(getCellVal(row, 6)).toBeDefined();
  }

  for (let row of denyPayment) {
    expect(getCellVal(row, 6)).toBeNull();
  }
});
