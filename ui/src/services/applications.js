import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getApplications = async (query = "") => {
  const { data } = await axios.get(
    `${userServiceBaseUrl}/applications${query}`
  );
  return data;
};
