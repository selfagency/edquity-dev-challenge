import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getPayments = async (query = "") => {
  const { data } = await axios.get(`${userServiceBaseUrl}/payments${query}`);
  return data;
};

export const createPayment = async ({ applicationUuid, requestedAmount }) => {
  const { data } = await axios.post(`${userServiceBaseUrl}/payments`, {
    applicationUuid,
    paymentAmount: requestedAmount
  });
  return data;
};
