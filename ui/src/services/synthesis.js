import axios from "axios";

const userServiceBaseUrl = "http://localhost:8080";

export const getSynthesis = async (query = "") => {
  const { data } = await axios.get(`${userServiceBaseUrl}/synthesis${query}`);
  return data;
};
