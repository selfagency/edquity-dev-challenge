import { Application, Payment, User } from "../db.js";
import formatCurrency from "../utils/formatCurrency.js";

class Synthesis {
  constructor() {
    this.state = null;
    this.setState();
  }
  setState() {
    const applications = Application.state;
    const payments = Payment.state;
    const users = User.state;

    this.state = users.map(({ uuid, name, email }) => {
      const { requestedAmount, uuid: applicationUuid } =
        applications.find((application) => application.userUuid === uuid) || {};
      const { paymentAmount, paymentMethod } =
        payments.find(
          (payment) => payment.applicationUuid === applicationUuid
        ) || {};

      // Format table data to be passed into the table component, pay button tacked
      // onto the end to allow payments to be issued for each row
      return {
        uuid,
        name,
        email,
        requestedAmount: formatCurrency(requestedAmount),
        paymentAmount: formatCurrency(paymentAmount),
        paymentMethod,
        initiatePayment: null,
        applicationUuid,
        eligible: requestedAmount && !paymentAmount
      };
    });
  }
  getCount() {
    return this.state.length;
  }
  getPage(offset, count) {
    this.setState();
    count = count < 0 ? undefined : offset + count;
    return this.state.slice(offset, count);
  }
  getAll() {
    this.setState();
    return this.state;
  }
  getById(uuid) {
    return this.state.find((item) => item.uuid === uuid);
  }
}

export default Synthesis;
