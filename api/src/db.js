import ApplicationModel from "./models/application.js";
import PaymentModel from "./models/payment.js";
import SynthesisModel from "./models/synthesis.js";
import UserModel from "./models/user.js";

const Application = new ApplicationModel();
const Payment = new PaymentModel();
const User = new UserModel();
const Synthesis = new SynthesisModel();

export { Application, Payment, User, Synthesis };
