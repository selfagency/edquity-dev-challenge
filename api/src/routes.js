import express from "express";
import { Application, Payment, Synthesis, User } from "./db.js";

// PAGINATION PARAMETER VALIDATION HELPER
// =============================================================================
const setParams = (query) => {
  let offset, count;

  // check if a record offset is defined and if not set it to 0
  try {
    offset = parseInt(query.offset);
    offset = Number.isInteger(offset) ? offset : 0;
  } catch {
    offset = 0;
  }

  // check if a record count is defined and if not set it to -1 (infinite)
  try {
    count = parseInt(query.count);
    count = Number.isInteger(count) ? count : -1;
  } catch {
    count = -1;
  }

  return [offset, count];
};

// ROUTES FOR OUR API
// =============================================================================
const router = express.Router();

// Define routes
const routes = [
  {
    path: "synthesis",
    model: Synthesis
  },
  {
    path: "users",
    model: User
  },
  {
    path: "applications",
    model: Application
  },
  {
    path: "payments",
    model: Payment
  }
];

// Instantiate routes
routes.forEach((route) => {
  router.get(`/${route.path}`, (req, res) => {
    // Set the offset and count parameters
    const [offset, count] = setParams(req.query);

    // Fetch the data
    const body = route.model.getPage(offset, count);

    // Get the page count
    const entries = route.model.getCount();
    const pageCount = Math.ceil(entries / count);

    // Get the current page number
    const current = Math.floor(offset / count) + 1;

    // Define the query for the previous page
    const prev = offset > 0 ? `?offset=${offset - count}&count=${count}` : null;

    // Define the query for the next page
    const next = `?offset=${offset + count}&count=${count}`;

    res.json({
      body,
      page: count
        ? { current, pageCount, entries, prev, next }
        : { current: 1, total: 1 }
    });
  });
});

router.post("/payments", (req, res) => {
  const body = Payment.create(req.body);
  res.json({ body });
});

export default router;
